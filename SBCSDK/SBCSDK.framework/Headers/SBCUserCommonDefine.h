//
//  SBCUserCommonDefine.h
//  SBCProject
//
//  Created by mac on 2020/12/3.
//  Copyright © 2020 ZhouYou. All rights reserved.
//

#ifndef SBCUserCommonDefine_h
#define SBCUserCommonDefine_h

//客户
#define kCustomerJumpUrl @"#min=1&limit=20&view_type=list&model=res.partner&action=280"

//销售项目
#define kSalesProjectJumpUrl @"#min=1&limit=20&view_type=list&model=bidding_mng.bidding_mng&menu_id=173&action=272"

//租赁项目
#define kLeaseProjectJumpUrl @"#min=1&limit=20&view_type=list&model=bidding_mng.bidding_mng&menu_id=173&action=272"
//借用项目
#define kBorrowProjectJumpUrl @"#min=1&limit=20&view_type=list&model=pd_stock.borrow&menu_id=503&action=720"
//交付项目
#define kGiveProjectJumpUrl @"#min=1&limit=20&view_type=list&model=project.project&menu_id=323&action=473"
//交流记录
#define kTalkRecordJumpUrl @"#min=1&limit=20&view_type=list&model=bidding_mng.communication_record&menu_id=552&action=816"
//方案评审
#define kProjectReviewJumpUrl @"#min=1&limit=20&view_type=list&model=bidding_mng.plan_review&menu_id=553&action=817"
//投标评审
#define kTenderReviewJumpUrl @"#min=1&limit=80&view_type=list&model=bidding_mng.review_project&menu_id=554&action=813"
//启动评审
#define kStartReviewJumpUrl @"#min=1&limit=80&view_type=list&model=bidding_mng.start_review_project&menu_id=555&action=815"

//事项推进
#define kProjectBoostJumpUrl @"#min=1&limit=20&view_type=list&model=project.affair_promote&menu_id=556&action=819"
//项目验收
#define kProjectCheckJumpUrl @"#min=1&limit=20&view_type=list&model=project.project_acceptance&menu_id=557&action=820"
//保证金/服务费
#define kServeiceMoneyJumpUrl @"#min=1&limit=20&view_type=list&model=bidding_mng.bidding_cost&menu_id=294&action=281"

//收款任务
#define kReceiptTaskJumpUrl @"#min=1&limit=20&view_type=list&model=gather_task.gather_task&menu_id=262&action=290"



#endif /* SBCUserCommonDefine_h */
