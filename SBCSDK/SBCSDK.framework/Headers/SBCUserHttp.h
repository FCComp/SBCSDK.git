//
//  SBCUserHttp.h
//  SBCSDK
//
//  Created by ZhouYou on 2020/1/7.
//  Copyright © 2020 ZhouYou. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBCBaseHttp.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBCUserHttp : NSObject

//登录 手机号+  验证码与密码二选一
+ (void)loginWithMobile:(NSString *)mobile code:(nullable NSString *)code password:(nullable NSString *)password result:(SBCRequestComplete)result;

//退出
+ (void)logOut:(SBCRequestComplete)result;

//发送验证码 type:login-登录 register-注册 update_password-设置/修改密码 update_trade_password-设置/修改交易密码
+ (void)sendSMSCodeWithMobile:(NSString *)mobile type:(NSString *)type result:(SBCRequestComplete)result;

//修改登录密码，参数：新密码+验证码
+ (void)updatePassword:(NSString *)password mobile:(NSString *)mobile smsCode:(NSString *)code result:(SBCRequestComplete)result;

//验证身份证号码
+ (void)checkUserValidateIdentity:(NSString *)indentity result:(SBCRequestComplete)result;

//修改交易密码  新交易密码+身份证号+验证码
+ (void)updateTradePassword:(NSString *)tradePassword identity:(NSString *)indentity smsCode:(NSString *)code result:(SBCRequestComplete)result;

//开通积分功能。tradePassword 交易密码
+ (void)openAccountWithPsd:(NSString *)tradePassword result:(SBCRequestComplete)result;;
@end

NS_ASSUME_NONNULL_END
