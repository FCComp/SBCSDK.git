//
//  SBCMessageDefine.h
//  SBCProject
//
//  Created by ZhouYou on 2019/12/26.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#ifndef SBCMessageDefine_h
#define SBCMessageDefine_h

//用户点击推送通知后
#define SBCMessageNotification_ReceivedNotification      @"SBCMessageNotification_ReceivedNotification"

//启动加载完成，启动页消失后由导航组件出发
#define SBCMessageNotification_DidFinishLoading          @"SBCMessageNotification_DidFinishLoading"

//注销登录,注销时发送此通知
#define SBCMessageNotification_DidLogout                 @"SBCMessageNotification_DidLogout"

#define SBCMessageNotification_DidFinishLaunching        @"SBCMessageNotification_DidFinishLaunching"

#define SBCMessageNotification_WillResignActive          @"SBCMessageNotification_WillResignActive"

#define SBCMessageNotification_DidEnterBackground        @"SBCMessageNotification_DidEnterBackground"

#define SBCMessageNotification_WillEnterForeground       @"SBCMessageNotification_WillEnterForeground"

#define SBCMessageNotification_DidBecomeActive           @"SBCMessageNotification_DidBecomeActive"

#define SBCMessageNotification_WillTerminate             @"SBCMessageNotification_WillTerminate"

//用户收到了工单系统的推送
#define SBCMessageNotification_ReceiveFCWorkOrderNoti            @"SBCMessageNotification_ReceiveFCWorkOrderNoti"

//用户token过期
#define SBCMessageNotification_TokenInValide  @"SBCMessageNotification_TokenInValide"
//接收到openURL消息
#define SBCAppDelegateHandleOpenURLNotification             @"SBCAppDelegateHandleOpenURLNotification"
#define SBCAppDelegateHandleOpenURLAndOpenUrlOptionsNotification            @"SBCAppDelegateHandleOpenURLAndOpenUrlOptionsNotification"

#define SBCAppDelegateHandleContinueUserActivityNotification                @"SBCAppDelegateHandleContinueUserActivityNotification"

#define SBCAppDelegateHandleContinueUserActivityNotification                @"SBCAppDelegateHandleContinueUserActivityNotification"

#endif /* SBCMessageDefine_h */
