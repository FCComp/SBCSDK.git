//
//  SBCAppDelegate+SBCJPush.h
//  SBCSDK
//
//  Created by ZhouYou on 2019/12/27.
//  Copyright © 2019 ZhouYou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBCAppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface SBCAppDelegate (SBCJPush)

@property (nonatomic, strong, nullable) NSDictionary *launchUserInfo;

- (void)SBCJPushApplication:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions;

- (void)SBCApplicationWillResignActive:(UIApplication *)application;

- (void)SBCApplicationDidEnterBackground:(UIApplication *)application;

- (void)SBCApplicationWillEnterForeground:(UIApplication *)application;

- (void)SBCApplicationDidBecomeActive:(UIApplication *)application;

- (void)SBCApplicationWillTerminate:(UIApplication *)application;

- (void)SBCApplication:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;

- (void)SBCApplication:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;

- (void)SBCApplication:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

@end

NS_ASSUME_NONNULL_END
